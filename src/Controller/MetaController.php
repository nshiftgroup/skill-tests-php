<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * This controller serves as an example of Symfony controllers
 */
class MetaController extends AbstractController
{

    /**
     * @Route("/status", name="status")
     */
    public function status()
    {
        return $this->json(["status" => "ok"]);
    }

}