<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MetaControllerTest extends WebTestCase
{

    public function testStatus()
    {
        $client = static::createClient();

        $client->request('GET', '/status');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $res = json_decode($client->getResponse()->getContent(), true);
        $this->assertArraySubset(["status" => "ok"], $res);
    }

}